var logger = require("./logger");
const DEDENT = require('dedent-js');

const Discord = require('discord.js');
class functions
{
 

    constructor()
    {
        this.discordClient = "";
        this.gitlabQueue = "";
        this.DaysOfWeek = ["Mon", "Tues", "Wed", "Thurs", "Fri", "Sat", "Sun"];
        this.MonthsOfYear = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        this.SETTINGS = {};
        this.WEBHOOKS = {};
        this.StrLen = {
            title: 128,
            description: 128,
            field_name: 128,
            field_value: 128,
            commit_id: 8,
            commit_msg: 32,
            json: 256,
            snippet_code: 256
          };
          
    }

    truncate(str, count, noElipses, noNewLines) {
        if (noNewLines) str = str.split('\n').join(' ');
        if (!count && str) return str;
        if (count && str && noElipses) {
            return str.substring(0, count);
        } else if (str && str.length > 0) {
            if (str.length <= count) return str;
            return str.substring(0, count - 3) + '...';
        } else {
            return "";
        }
    }

    FormatMessage(webBody,webHeaders)
    {
        if(webBody.object_kind=="build")
        {
            return this.FormatBuild(webBody);
        }
        else if(webBody.object_kind=="issue")
        {
            return this.FormatIssue(webBody);
        }
        else if(webBody.object_kind=="merge_request")
        {
            return this.FormatMerge(webBody);
        }
        else if(webBody.object_kind=="note")
        {
            if(webBody.object_attributes.noteable_type=="Issue")
            {
                return this.FormateNoteIssue(webBody);
            }
            else if(webBody.object_attributes.noteable_type=="Commit")
            {
                return this.FormatNoteCommit(webBody);
            }
            else if(webBody.object_attributes.noteable_type=="MergeRequest")
            {
                return this.FormatNoteMerge(webBody);
            }
            else if(webBody.object_attributes.noteable_type=="Snippet")
            {
                return this.FormatNoteSnippet(webBody);
            }
            else
            {
                return "";
            }
        }
        else if(webBody.object_kind == "pipeline")
        {
            return this.FormatPipeline(webBody);
        }
        else if(webBody.object_kind=="push")
        {
            return this.FormatPush(webBody);
        }
        else if(webBody.object_kind=="tag_push")
        {
            return this.FormatTag(webBody);
        }
        else if(webBody.object_kind=="wiki_page")
        {
            return this.FormatWiki(webBody);
        }
        else
        {
            return "";
        }
    }

    FormatPush(webBody)
    {
        var datestamp = new Date();
        let embed = {
            color: this.SETTINGS.gitlab_hook_colors["commit"],
            author: {
              name: webBody.user_username,
              icon_url: webBody.user_avatar
            },
            title: "",
            url: webBody.project.web_url,
            description:"",
            fields: {},
            timestamp: datestamp,
            footer: {
                icon_url: this.discordClient.user.avatarURL,
                text: this.discordClient.user.username + " " + this.DaysOfWeek[datestamp.getDay()-1
                ] + " " + this.MonthsOfYear[datestamp.getMonth()] + " " + datestamp.getDate() + " " + datestamp.getFullYear()
            }
          };
        
        embed.title = `[${webBody.project.path_with_namespace}]`;
        if(webBody.total_commits_count == 1)
        {
            
            embed.description = DEDENT `
            **1 Commit**
            [${this.truncate(webBody.commits[0].id,this.StrLen.commit_id,true)}](${webBody.commits[0].url})
            ${webBody.commits[0].message}
            ${webBody.commits[0].modified.length} change(s)
            ${webBody.commits[0].added.length} addition(s)
            ${webBody.commits[0].removed.length} deletion(s)
            `;
        }
        else if(webBody.total_commits_count > 1)
        {
            embed.description = `**${webBody.total_commits_count} Commits**\n`;
            for(var i = 0; i < webBody.commits.length; i++)
            {
                embed.description += `[${this.truncate(webBody.commits[i].id,this.StrLen.commit_id,true)}](${webBody.commits[i].url}) ${this.truncate(webBody.commits[i].message,this.StrLen.commit_msg).trim()} - ${webBody.commits[i].author.name}\n`;
            }
        }
        else
        {
            return "";
        }

        var myembed = new Discord.RichEmbed()
            .setTitle(embed.title)
            .setAuthor(embed.author.name, embed.author.icon_url, "https://gitlab.com/" + webBody.user_username)
            .setColor(embed.color)
            .setDescription(embed.description)
            .setFooter(embed.footer.text, embed.footer.icon_url)
            .setThumbnail(embed.author.icon_url)
            .setTimestamp()
            .setURL(embed.url)
            ;
        
        return myembed;
    }

    FormatBuild(webBody)
    {
        return "";
    }

    FormatIssue(webBody)
    {
        return "";
    }

    FormatMerge(webBody)
    {
        return "";
    }

    FormatNoteCommit(webBody)
    {
        return "";
    }

    FormatNoteMerge(webBody)
    {
        return "";
    }

    FormateNoteIssue(webBody)
    {
        return "";
    }

    FormatNoteSnippet(webBody)
    {
        return "";
    }

    FormatPipeline(webBody)
    {
        return "";
    }

    FormatTag(webBody)
    {
        var datestamp = new Date();
        let embed = {
            color: this.SETTINGS.gitlab_hook_colors["commit"],
            author: {
              name: webBody.user_username,
              icon_url: webBody.user_avatar
            },
            title: "",
            url: webBody.project.web_url + webBody.ref.substring('refs'.length),
            description:"",
            fields: {},
            timestamp: datestamp,
            footer: {
                icon_url: this.discordClient.user.avatarURL,
                text: this.discordClient.user.username + " " + this.DaysOfWeek[datestamp.getDay()-1
                ] + " " + this.MonthsOfYear[datestamp.getMonth()] + " " + datestamp.getDate() + " " + datestamp.getFullYear()
            }
          };
        
        embed.title = "[" + webBody.project.path_with_namespace + "]";
        embed.description = "**Tag " + webBody.ref.substring('refs/tags/'.length) + "**\n";
        if(webBody.total_commits_count == 1)
        {
            embed.description += DEDENT `
            [${this.truncate(webBody.commits[0].id,this.StrLen.commit_id,true)}](${webBody.commits[0].url})
            ${webBody.commits[0].message}
            ${webBody.commits[0].modified.length} change(s)
            ${webBody.commits[0].added.length} addition(s)
            ${webBody.commits[0].removed.length} deletion(s)
            `;
        }
        else if(webBody.total_commits_count > 1)
        {
            
            for(var i = 0; i < webBody.commits.length; i++)
            {
                embed.description += `[${this.truncate(webBody.commits[i].id,this.StrLen.commit_id,true)}](${webBody.commits[i].url}) ${this.truncate(webBody.commits[i].message,this.StrLen.commit_msg).trim()} - ${webBody.commits[i].author.name}\n`;
            }
        }
        else
        {
            return "";
        }
  
        var myembed = new Discord.RichEmbed()
            .setTitle(embed.title)
            .setAuthor(embed.author.name, embed.author.icon_url, "https://gitlab.com/" + webBody.user_username)
            .setColor(embed.color)
            .setDescription(embed.description)
            .setFooter(embed.footer.text, embed.footer.icon_url)
            .setThumbnail(embed.author.icon_url)
            .setTimestamp()
            .setURL(embed.url)
            .addField('Previous',`[${this.truncate(webBody.before, this.StrLen.commit_id, true)}](${webBody.project.web_url}/commit/${webBody.before} 'Check the previous tagged commit')`,true)
            .addField('Current',`[${this.truncate(webBody.after, this.StrLen.commit_id, true)}](${webBody.project.web_url}/commit/${webBody.after} 'Check the current tagged commit')`,true)
            ;
        
        return myembed;
    }

    FormatWiki(webBody)
    {
        return "";
    }

}
var GlobalFunctions = (function () {
    var instance;

    function createInstance() {
        var object = new functions();
        return object;
    }

    return {
        getInstance: function () {
            if (!instance) {
                instance = createInstance();
            }
            return instance;
        }
    };
})();
module.exports = GlobalFunctions;

var fs = require("fs");
var logger = require("./logger");
GlobalFunctions = require('./functions');
const api_v1 = require("express").Router();

//Load settings configuration
var contents = fs.readFileSync("config.json");
var SETTINGS = JSON.parse(contents);

/*Index page*/
api_v1.get('/',(req,res)=>{
    res.status(200).json({message:'connected'});
});

api_v1.post('/gitlabhook/',(req,res)=>{
    res.send('{"response":{"data":"Notified" } }');
    GlobalFunctions.getInstance().gitlabQueue.create('request',{webData : req.body, webHeaders : req.headers}).save();
});


module.exports=api_v1;

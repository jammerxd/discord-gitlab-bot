var cluster = require('cluster');
if(cluster.isMaster)
{
    var i = 0;
    for(i; i < 1; i++)
    {
        cluster.fork();
    }
    cluster.on('exit',function(worker){
        console.log('Worker ' + worker.id + ' died.');
        cluster.fork();
    });
}
else
{

var fs = require("fs");
var logger = require("./logger");
var path = require("path");
process.chdir(path.dirname(fs.realpathSync(__filename)));
var minimumTLSVersion = require('minimum-tls-version');
var request = require("sync-request");
var htmlparser = require("htmlparser");
var bodyParser = require('body-parser');
const Discord = require('discord.js');
const DEDENT = require('dedent-js');
var GlobalFunctions = require('./functions');
GlobalFunctions.getInstance().discordClient = new Discord.Client();

var kue = require('kue');
GlobalFunctions.getInstance().gitlabQueue = kue.createQueue();

//Load settings configuration
var contents = fs.readFileSync("config.json");
GlobalFunctions.getInstance().SETTINGS = JSON.parse(contents);



var cf = require('node_cloudflare');


var app = require("express")();
var api_v1 = require("./v1_routing");


var options = {
    key: fs.readFileSync(GlobalFunctions.getInstance().SETTINGS.ssl_key),
    cert: fs.readFileSync(GlobalFunctions.getInstance().SETTINGS.ssl_crt),
    pingTimeout: 60000,
    pingInterval: 25000,
    secureOptions: minimumTLSVersion('tlsv12')
};

////---------------------------------------App POST Handling-------------------------------------\\
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
//--------------------------------------------------------\\

////---------------------------------------API Routing-------------------------------------\\
app.get('/', function (req, res) {
    res.send("Hello World!");
});
app.use('/api',api_v1);
app.use('/api/v1',api_v1);
/*
app.post('/api/v:version/test',function(req,res){
    var  test_item = req.body.test;
    res.send("Requested api version: " + req.params.version + "\nTest Item: " + test_item + "\nTest Status: PASS");
});*/

//--------------------------------------------------------\\



var https = require("https").createServer(options, app);

//--------------------SIGTERM FUNCTION--------------------\\
//#region Setup SIGTERM signal
process.on("SIGTERM", function () {
    logger.info("Server closed.");
    //TODO: Save any temporary data...
    process.exit(0);

});
//#endregion
//--------------------------------------------------------\\

//--------------------PROCESS UNCAUGHT EXCEPTION FUNCTION--------------------\\
//#region Setup uncaught exception
process.on("uncaughtException", function () {
    logger.info("Server closed due to uncaught exception.");
    //TODO: Save any temporary data...
    process.exit(1);

});
//#endregion
//--------------------------------------------------------\\


//--------------------------------------Discord Bot Configuration------------------------------------\\
function SetupDiscordBot()
{
    logger.info("Setting up discord bot!");
    GlobalFunctions.getInstance().discordClient.on('ready', ()=>{
    
        for(var key in GlobalFunctions.getInstance().SETTINGS.webhooks)
        {
            GlobalFunctions.getInstance().WEBHOOKS[key] = GlobalFunctions.getInstance().discordClient.guilds.find(x=> x.id === GlobalFunctions.getInstance().SETTINGS.webhooks[key]["server_id"]).channels.find(x=>x.id===GlobalFunctions.getInstance().SETTINGS.webhooks[key]["channel_id"]);
        }

        logger.info("Discord Bot is ready!");
        GlobalFunctions.getInstance().gitlabQueue.process('request',function (job, done){
            //check if the request is valid
            var valid = false;
            try{
                if(job.data.webHeaders["x-gitlab-token"] != false && job.data.webHeaders["x-gitlab-token"] != "" && job.data.webHeaders["x-gitlab-event"] != false && job.data.webHeaders["x-gitlab-event"] != "")
                {
                    if(GlobalFunctions.getInstance().SETTINGS.repository_mappings[job.data.webHeaders["x-gitlab-token"]] != false && GlobalFunctions.getInstance().SETTINGS.repository_mappings[job.data.webHeaders["x-gitlab-token"]] != "")
                    {
                        valid = true;
                    }
                }
            }
            catch{
                
            }
            
            if(valid)
            {
                var message = GlobalFunctions.getInstance().FormatMessage(job.data.webData,job.data.webHeaders);
                if(message != "")
                {
                    for(var i = 0; i < GlobalFunctions.getInstance().SETTINGS.repository_mappings[job.data.webHeaders["x-gitlab-token"]].length; i++)
                    {
                        var webhook_name = GlobalFunctions.getInstance().SETTINGS.repository_mappings[job.data.webHeaders["x-gitlab-token"]][i];
                        GlobalFunctions.getInstance().WEBHOOKS[webhook_name].send(message);
                        
                    }
                }
            }
            done();
        });
        logger.info("Ready for jobs!");
    });
    GlobalFunctions.getInstance().discordClient.on('disconnected',function(){
        Console.log("Bot disconnected.");
        process.exit(1);
    });
    GlobalFunctions.getInstance().discordClient.login(GlobalFunctions.getInstance().SETTINGS.discord_bot_token);
}
//---------------------------------------------------------------------------------------------\\


//--------------------------------------HTTPS Server Configuration------------------------------------\\
function SetupHTTPSServer()
{
    https.listen(GlobalFunctions.getInstance().SETTINGS.server_port,GlobalFunctions.getInstance().SETTINGS.server_bind);
    https.on('listening',function(){
        logger.info("Gitlab Monitor Bot Started. PID: " + process.pid);
        SetupDiscordBot();
    });
}
//--------------------------------------------------------\\


//---------------------------------------Cloudflare config-------------------------------------\\
cf.load(function (error, fs_error) {
    if (fs_error) {
        throw new Error(fs_error);
    }

    SetupHTTPSServer();

});
}//end of cluster else statement